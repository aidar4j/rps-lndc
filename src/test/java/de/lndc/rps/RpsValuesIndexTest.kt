package de.lndc.rps

import de.lndc.rps.dto.RpsValues
import junit.framework.Assert.assertEquals
import org.junit.Test

class RpsValuesIndexTest {

  @Test
  fun checkRock() {
    val res = RpsValues.values()[0].toString()

    assertEquals(res, "ROCK")
  }

  @Test
  fun checkPaper() {
    val res = RpsValues.values()[1].toString()

    assertEquals(res, "PAPER")
  }

  @Test
  fun checkScissors() {
    val res = RpsValues.values()[2].toString()

    assertEquals(res, "SCISSORS")
  }
}
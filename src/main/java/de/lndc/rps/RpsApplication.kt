package de.lndc.rps

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class RpsApplication

fun main(args: Array<String>) {
	runApplication<RpsApplication>(*args)
}

package de.lndc.rps.dto

data class RpsDto(
    val result: RpsValues
)

data class PlayerDto(
    val id: Int,
    val value: RpsValues,
    var winner: Boolean
)

enum class RpsValues {
  ROCK, PAPER, SCISSORS
}
package de.lndc.rps.dto

data class BottleDto(
    val winner: Int
)
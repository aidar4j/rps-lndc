package de.lndc.rps.dto

data class CoinDto(
    val result: Boolean
)
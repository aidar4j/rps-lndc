package de.lndc.rps.controller

import de.lndc.rps.dto.CoinDto
import de.lndc.rps.service.CoinFlipperService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/coins")
class CoinFlipperController(
    private val coinFlipperService: CoinFlipperService
) {

  @PostMapping
  fun spin(): CoinDto = coinFlipperService.flip()


}
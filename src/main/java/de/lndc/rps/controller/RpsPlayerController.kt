package de.lndc.rps.controller

import de.lndc.rps.dto.PlayerDto
import de.lndc.rps.service.RpsPlayerService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/rps")
class RpsPlayerController(
    private val rpsPlayerService: RpsPlayerService
) {

  @PostMapping("/{idsOfPlayers}")
  fun play(@PathVariable idsOfPlayers: List<Int>)
      : List<PlayerDto> = rpsPlayerService.play(idsOfPlayers)

}
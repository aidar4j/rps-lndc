package de.lndc.rps.controller

import de.lndc.rps.dto.BottleDto
import de.lndc.rps.service.BottleSpinnerService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/bottle")
class BottleSpinnerController(
    private val bottleSpinnerService: BottleSpinnerService
) {

  @PostMapping("/{amount}")
  fun spin(@PathVariable amount: Int)
      : BottleDto = bottleSpinnerService.spin(amount)
}
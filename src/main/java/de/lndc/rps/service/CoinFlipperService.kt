package de.lndc.rps.service

import de.lndc.rps.dto.CoinDto
import org.springframework.stereotype.Service
import kotlin.random.Random

interface CoinFlipperService {
  fun flip(): CoinDto
}

@Service
class DefaultCoinFlipperService : CoinFlipperService {

  override fun flip(): CoinDto {
    val result: Boolean = Random.nextBoolean()

    return CoinDto(
        result = result
    )
  }
}
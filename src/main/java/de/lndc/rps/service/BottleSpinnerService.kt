package de.lndc.rps.service

import de.lndc.rps.dto.BottleDto
import org.springframework.stereotype.Service
import kotlin.random.Random

interface BottleSpinnerService {
  fun spin(amount: Int): BottleDto
}

@Service
class DefaultBottleSpinnerService : BottleSpinnerService {

  override fun spin(amount: Int): BottleDto {
    val winner = Random.nextInt(amount)

    return BottleDto(winner = winner)
  }

}
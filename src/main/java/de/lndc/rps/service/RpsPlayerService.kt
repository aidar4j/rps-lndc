package de.lndc.rps.service

import de.lndc.rps.dto.PlayerDto
import de.lndc.rps.dto.RpsValues
import org.springframework.stereotype.Service
import kotlin.random.Random

interface RpsPlayerService {
  fun play(idsOfPlayers: List<Int>): List<PlayerDto>
}

@Service
class DefaultRpsPlayerService : RpsPlayerService {

  override fun play(idsOfPlayers: List<Int>): List<PlayerDto> {
    val playerDtos = getPlayerDtos(idsOfPlayers)

    if (playerDtos.map { it.value }.containsAll(RpsValues.values().toList())) {
      playerDtos.forEach {
        it.winner = true
      }

      return playerDtos
    }

    if (isAllEqual(playerDtos.map { it.value })) {
      playerDtos.forEach {
        it.winner = true
      }

      return playerDtos
    }

    playerDtos.forEach {
      if (it.value == RpsValues.ROCK && playerDtos.map { x -> x.value }.contains(RpsValues.SCISSORS)) {
        it.winner = true
      }

      if (it.value == RpsValues.PAPER && playerDtos.map { x -> x.value }.contains(RpsValues.ROCK)) {
        it.winner = true
      }

      if (it.value == RpsValues.SCISSORS && playerDtos.map { x -> x.value }.contains(RpsValues.PAPER)) {
        it.winner = true
      }
    }

    return playerDtos
  }

  private fun getPlayerDtos(idsOfPlayers: List<Int>): List<PlayerDto> =
      idsOfPlayers.map {
        val index = Random.nextInt(RpsValues.values().size)
        val result = RpsValues.values()[index]

        PlayerDto(
            id = it,
            value = result,
            winner = false
        )
      }

  private fun isAllEqual(a: List<RpsValues>): Boolean {
    for (i in 1 until a.size) {
      if (a[0] != a[i]) {
        return false
      }
    }

    return true
  }
}